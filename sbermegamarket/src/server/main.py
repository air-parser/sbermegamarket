from fastapi import FastAPI, APIRouter, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles


templates = Jinja2Templates(directory='templates')

app = FastAPI(
    title="Recipe API",
)

app.mount("/static", StaticFiles(directory="static"), name="static")

api_router = APIRouter()


@api_router.get("/sbermegamarket/category/{parent_id}", status_code=200)
def root(request: Request, parent_id: str = '0'):
    return templates.TemplateResponse(
        'sbermegamarket/category.html',
        {'request': request, 'parent_id': parent_id}
    )


@api_router.get("/sbermegamarket/search/{collection_id}", status_code=200)
def root(request: Request, collection_id: str = '0', limit: int = 44, offset: int = 0):
    return templates.TemplateResponse(
        'sbermegamarket/search.html',
        {'request': request, 'collection_id': collection_id, 'limit': limit, 'offset': offset}
    )


app.include_router(api_router)


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port = 8000, log_level = "debug")

