from settings import settings
from playwright.async_api import PlaywrightContextManager, Page


class Spider:

    def __init__(self, page: Page = None,  **kwargs):
        self.page = page
        self.browser = None
        self.browser_settings = kwargs.get('browser_settings', {})
        self.playwright_context_manager = PlaywrightContextManager()

    async def create(self):
        playwright = await self.playwright_context_manager.start()

        slow_mo = self.browser_settings.get('slow_mo')
        timeout = self.browser_settings.get('timeout')
        headless = self.browser_settings.get('timeout')

        self.browser = await playwright.firefox.launch(
            headless = headless if headless else settings.browser.headless,
            slow_mo = slow_mo if slow_mo else settings.browser.slow_mo,
            timeout = timeout if timeout else settings.browser.timeout,
        )
        return self

    async def __aenter__(self):
        self.context = await self.browser.new_context()
        return self

    async def __aexit__(self, *args):
        await self.browser.close()
        await self.playwright_context_manager.__aexit__()
